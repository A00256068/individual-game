var vec_x = argument0;
var vec_y = argument1;
var max_length = argument2;

var length = vector_length(vec_x,vec_y);

if(length != 0)
{
    vec_x /= length;
}

var new_length = min(max_length, length);
return (vec_x * new_length);
